GOARCH ?= amd64

.PHONY: ci-setup
ci-setup: 
	git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
	mkdir -p ${CI_PROJECT_DIR}/.cache
	export GOPATH=${CI_PROJECT_DIR}/.cache

.PHONY: build-cup-svc-linux
build-cup-svc-linux:
	GOOS=linux $(MAKE) build-cup-svc

.PHONY: build-cup-svc
build-cup-svc:
	mkdir -p build
	CGO_ENABLED=0 installsuffix=cgo go build -ldflags "-extldflags '-static'" -o ./cmd/cup-svc/cup-svc-$(GOOS)-$(GOARCH) ./cmd/cup-svc
	strip --strip-all ./cmd/cup-svc/cup-svc-$(GOOS)-$(GOARCH)

.PHONY: docker-cup-svc-linux
docker-cup-svc-linux: build-cup-svc-linux
	docker build cmd/cup-svc