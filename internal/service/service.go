package service

import (
	"fmt"
	"os"

	"google.golang.org/grpc"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/timeterm/cup-svc/internal/database"
	"gitlab.com/timeterm/cup-svc/internal/database/daos"
	"gitlab.com/timeterm/env"
	userpb "gitlab.com/timeterm/proto/go/user"
)

const (
	envModeDev     = "MODE_DEV"
	envUserSvcAddr = "USER_SVC_ADDR"
)

func init() {
	env.RegisterSpec(NewBaseEnvSpec())
}

// Base defines the base of the service. For now it only contains a database and DAO registry.
type Base struct {
	DB  *database.Database
	Reg daos.Registry
	UC  userpb.UserClient
}

// NewBase creates a new service base containing a database and DAO registry.
// It also initializes a tracer and logger at the global level, and checks if all required environment
// variables are resolved as to satisfaction. Base.Close should be called at shutdown of owner.
func NewBase() (Base, error) {
	var base Base

	// Make sure we always log the caller.
	log.Logger = log.With().Caller().Logger()

	// Check if the service is configured in development mode - and if so -
	// make the logs more readable for the developer.
	// Checking this error is not necessary as "MODE_DEV" is not required
	// and the only env we're looking for.
	devEnvs, _ := NewBaseEnvSpec().Resolve()
	if devEnvs[envModeDev] == "true" {
		log.Logger = log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	// Resolve all environment variables and quit if required environment variables
	// are not set.
	envs, err := env.GlobalSpec().Resolve()
	if err != nil {
		return base, err
	}

	// Create a connection with the user-svc for session checking and user information
	// retrieval.
	uconn, err := grpc.Dial(envs[envUserSvcAddr], grpc.WithInsecure())
	if err != nil {
		return base, fmt.Errorf("could not dial user-svc: %w", err)
	}
	uc := userpb.NewUserClient(uconn)

	// Open a connection with Redis.
	db, err := database.Open()
	if err != nil {
		return base, fmt.Errorf("could not open database: %w", err)
	}

	// Initialize the DAOs.
	reg, err := daos.NewRegistry(db)
	if err != nil {
		return base, fmt.Errorf("could not create DAO registry: %w", err)
	}

	return Base{
		DB:  db,
		Reg: reg,
		UC:  uc,
	}, nil
}

// Close closes the service (the contained database).
func (b *Base) Close() error {
	return b.DB.Close()
}

func NewBaseEnvSpec() env.Spec {
	return env.Spec{
		{
			Name:     envUserSvcAddr,
			Required: true,
		},
		{
			Name:     envModeDev,
			Value:    "false",
			Help:     "To act in development mode or not",
			Required: false,
		},
	}
}
