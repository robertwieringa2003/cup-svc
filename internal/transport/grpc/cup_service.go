package grpctx

import "gitlab.com/timeterm/cup-svc/internal/service"

// CUPServer implements CUPServer from the generated Go code from
// the Protocol Buffers definition of the CUP service.
type CUPServer struct {
	base *service.Base
}

// NewCUPServer creates a new server implementing CUPServer from
// the generated Go code from the Protocol Buffers CUP service definition.
func NewCUPServer(b *service.Base) CUPServer {
	return CUPServer{base: b}
}
