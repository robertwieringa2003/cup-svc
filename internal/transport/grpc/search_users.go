package grpctx

import (
	"context"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// SearchUsers searches for users in CUP. Note that this must be true: 3 <= len(req.FirstLetters) <= 7.
// See the equivalent in cupsoup: cupsoup.Client.SearchUsers.
func (c CUPServer) SearchUsers(ctx context.Context, req *pb.SearchUsersRequest) (*pb.SearchUsersResponse, error) {
	numFirstLetters := len(req.GetFirstLetters())
	if numFirstLetters < 3 || numFirstLetters > 7 {
		return nil, status.Error(codes.InvalidArgument, "expected 3 to 7 letters")
	}

	cl := cupsoup.NewClient()
	err := cl.Init(ctx)
	if err != nil {
		log.Error().Err(err).Msg("could not initialize the cupsoup client")
		return nil, status.Error(codes.Internal, "could not initialize the CUP client")
	}

	users, err := cl.SearchUsers(ctx, req.GetFirstLetters())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "could not search users: %v", err)
	}

	rsp := pb.SearchUsersResponse{
		Users: make([]string, len(users)),
	}

	var i int
	for _, user := range users {
		rsp.Users[i] = user
		i++
	}

	return &rsp, nil
}
