package grpctx

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"gitlab.com/timeterm/cup-svc/internal/transport/grpc/convert"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// Choose chooses an option (for a period) in CUP. See the equivalent in cupsoup: cupsoup.Client.Choose.
func (c CUPServer) Choose(ctx context.Context, req *pb.ChooseRequest) (*pb.ChooseResponse, error) {
	sess, err := convert.SessionProtoToCupsoup(req.GetSession())
	if err != nil {
		return nil, err
	}

	cl := cupsoup.NewClient(cupsoup.WithSession(sess))

	err = cl.Choose(ctx, int(req.InternalId))
	if err != nil {
		if ce, ok := err.(*cupsoup.ChooseError); ok {
			err = grpc.SetTrailer(ctx, metadata.MD{
				"cup-error": {ce.Reason},
			})
			if err != nil {
				return nil, err
			}
		}
		return nil, err
	}

	protoSess, err := convert.SessionCupsoupToProto(cl.Session())
	if err != nil {
		return nil, err
	}

	return &pb.ChooseResponse{
		Session: protoSess,
	}, nil
}
