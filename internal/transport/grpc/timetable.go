package grpctx

import (
	"context"

	"gitlab.com/timeterm/cup-svc/internal/transport/grpc/convert"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// Timetable retrieves the longest timetable span retrievable and returns it.
// See the equivalent in cupsoup: cupsoup.Client.Timetable.
func (c CUPServer) Timetable(ctx context.Context, req *pb.TimetableRequest) (*pb.TimetableResponse, error) {
	sess, err := convert.SessionProtoToCupsoup(req.GetSession())
	if err != nil {
		return nil, err
	}

	cl := cupsoup.NewClient(cupsoup.WithSession(sess))

	pt, err := cl.Timetable(ctx)
	if err != nil {
		return nil, err
	}

	var i int
	weeks := make([]*pb.TimetableWeek, len(pt))

	for week, appointments := range pt {
		newAppointments := make([]*pb.HistoryOption, len(appointments))
		for i := range appointments {
			var histOpt *pb.HistoryOption
			histOpt, err = convert.HistoryOptionCupsoupToProto(&appointments[i])
			if err != nil {
				return nil, err
			}

			newAppointments[i] = histOpt
		}

		weeks[i] = &pb.TimetableWeek{
			Week:         int32(week),
			Appointments: newAppointments,
		}
		i++
	}

	protoSess, err := convert.SessionCupsoupToProto(cl.Session())
	if err != nil {
		return nil, err
	}

	return &pb.TimetableResponse{
		Session: protoSess,
		Weeks:   weeks,
	}, nil
}
