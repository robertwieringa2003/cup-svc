package grpctx

import (
	"context"

	"github.com/gogo/protobuf/types"
	"github.com/rs/zerolog/log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/timeterm/cup-svc/internal/database/daos"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// GetUser retrieves a user's CUP name selection from the database.
func (c CUPServer) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.GetUserResponse, error) {
	d, ok := daos.GetUserDAO(c.base.Reg)
	if !ok {
		// Logging here is fine since the error the returned error is passed to the client, but the server should
		// still be notified of the error.
		log.Error().Msg("could not get user dao")

		return nil, status.Error(codes.Internal, "could not reach database")
	}

	sess, err := c.validateSession(ctx, req.GetTtSessionId())
	if err != nil {
		return nil, err
	}

	cupName, err := d.GetCUPName(sess.GetCardId())
	if err != nil {
		return nil, status.Error(codes.NotFound, "user not found")
	}

	return &pb.GetUserResponse{
		CupName: cupName,
	}, nil
}

// SetUser sets a user's CUP name selection in the database.
func (c CUPServer) SetUser(ctx context.Context,
	req *pb.SetUserRequest,
) (*types.Empty, error) {
	d, exists := daos.GetUserDAO(c.base.Reg)
	if !exists {
		log.Error().Msg("could not get user dao")

		return nil, status.Error(codes.Internal, "could not reach database")
	}

	sess, err := c.validateSession(ctx, req.GetTtSessionId())
	if err != nil {
		return nil, err
	}

	err = d.Set(sess.GetCardId(), req.GetCupName())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.Empty{}, nil
}
