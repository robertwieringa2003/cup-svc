package grpctx

import (
	"net"

	"google.golang.org/grpc"

	"gitlab.com/timeterm/cup-svc/internal/service"
	"gitlab.com/timeterm/env"
	pb "gitlab.com/timeterm/proto/go/cup"
)

const envListenAddr = "LISTEN_ADDR"

func init() {
	env.RegisterSpec(ListenEnvSpec())
}

// Listen creates a new TCP listener at LISTEN_ADDR, listening for gRPC
// client connections.
func Listen(b *service.Base) error {
	envs, err := ListenEnvSpec().Resolve()
	if err != nil {
		return err
	}

	lis, err := net.Listen("tcp", envs[envListenAddr])
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()
	pb.RegisterCUPServer(grpcServer, NewCUPServer(b))

	return grpcServer.Serve(lis)
}

// ListenEnvSpec specifies the required environment variables for calling
// Listen. Listen resolves these itself.
func ListenEnvSpec() env.Spec {
	return env.Spec{
		env.Var{
			Name:  envListenAddr,
			Value: "localhost:12345",
		},
	}
}
