package convert

import (
	"github.com/gogo/protobuf/types"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// AppointmentCupsoupToProto converts model.Appointment to a pb.Appointment.
func AppointmentCupsoupToProto(a *model.Appointment) (*pb.Appointment, error) {
	startDate, err := types.TimestampProto(a.StartDate)
	if err != nil {
		return nil, err
	}

	endDate, err := types.TimestampProto(a.EndDate)
	if err != nil {
		return nil, err
	}

	var selected *pb.Option
	if a.Selected != nil {
		selected = OptionCupsoupToProto(*a.Selected)
	}

	choices := make([]*pb.Choice, len(a.Choices))
	for i, choice := range a.Choices {
		choices[i] = ChoiceCupsoupToProto(choice)
	}

	return &pb.Appointment{
		Fixed:     a.Fixed,
		Slot:      int32(a.Slot),
		StartDate: startDate,
		EndDate:   endDate,
		Selected:  selected,
		Choices:   choices,
	}, nil
}
