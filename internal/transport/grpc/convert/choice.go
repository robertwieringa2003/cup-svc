package convert

import (
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// ChoiceCupsoupToProto converts model.Choice to a pb.Choice.
func ChoiceCupsoupToProto(c model.Choice) *pb.Choice {
	return &pb.Choice{
		Option:     OptionCupsoupToProto(c.Option),
		Full:       c.Full,
		InternalId: int32(c.InternalID),
	}
}
