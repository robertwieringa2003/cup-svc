package database

import (
	"github.com/go-redis/redis"
	"gitlab.com/timeterm/env"
)

const envDatabaseAddr = "DATABASE_ADDR"

func init() {
	env.RegisterSpec(OpenEnvSpec())
}

// Database is a wrapper around `redis.Client` providing utilities for using hashes
// with UnmarshalText and MarshalText.
type Database struct {
	*redis.Client
}

// Open opens the Database with the address specified in DATABASE_ADDR.
// The root of the program calling this function is expected to have checked all environment variables to exist as to
// guarantee no early return due to missing environment variables. This can be done using env.GlobalSpec().Resolve().
func Open() (*Database, error) {
	envs, err := OpenEnvSpec().Resolve()
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(&redis.Options{
		Addr: envs[envDatabaseAddr],
	})

	// Check whether the database is working or not.
	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}

	return &Database{client}, nil
}

// OpenEnvsSpec returns the environment variable specification. This can be used for narrowly resolving the required
// environment variables for opening the database.
func OpenEnvSpec() env.Spec {
	return env.Spec{
		env.Var{
			Name:     envDatabaseAddr,
			Required: true,
		},
	}
}
