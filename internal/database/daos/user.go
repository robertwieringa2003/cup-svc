package daos

import (
	"strconv"

	"gitlab.com/timeterm/cup-svc/internal/database"
)

const collectionUser = "cup-svc_user"

// UserDAO is a data access object for storing and retrieving user configuration.
type UserDAO struct {
	db *database.Database
}

// GetUserDAO from Registry r. If the DAO does not exist, or the "user" collection
// is not associated with a UserDAO,  false is returned. Only assert that
// the UserDAO is valid if ok is true.
func GetUserDAO(r Registry) (u *UserDAO, ok bool) {
	dao, exists := r.GetDAO(collectionUser)
	if !exists {
		return nil, false
	}

	userDAO, ok := dao.(*UserDAO)
	return userDAO, ok
}

// Init initializes the UserDAO by setting the database to read and write from.
func (u *UserDAO) Init(db *database.Database) {
	u.db = db
}

// Collection returns the (hash)map to store the values of the UserDAO in.
func (u *UserDAO) Collection() string {
	return collectionUser
}

// GetCUPName retrieves a user's cup name by its card ID.
// This is usually a student number or a teacher code.
func (u *UserDAO) GetCUPName(cardID uint32) (string, error) {
	idStr := strconv.FormatUint(uint64(cardID), 16)

	cupName, err := u.db.HGet(collectionUser, idStr).Result()
	if err != nil {
		return "", err
	}
	return cupName, nil
}

// Set sets the preferences of a user.
func (u *UserDAO) Set(id uint32, cupName string) error {
	idStr := strconv.FormatUint(uint64(id), 16)
	return u.db.HSet(collectionUser, idStr, cupName).Err()
}
