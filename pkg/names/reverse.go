package names

import (
	"bytes"
	"strings"
)

// Reverse reverses a name like 'Plop van der Kabouterbos' to 'Kabouterbos van der Plop'.
// This should the way names are stored in CUP.
func Reverse(name string) string {
	parts := strings.Split(name, " ")

	switch len(parts) {
	case 0:
		return ""
	case 1:
		return parts[0]
	}

	var b bytes.Buffer

	// The (last part of the) surname.
	b.WriteString(parts[len(parts)-1])

	// Add the family name affix in the original order (like 'van der').
	for i := 1; i < len(parts)-1; i++ {
		if len(parts)-1 < 3 || i > 1 {
			b.WriteByte(' ')
		}
		b.WriteString(parts[i])
	}

	b.WriteByte(' ')
	b.WriteString(parts[0])

	return b.String()
}
