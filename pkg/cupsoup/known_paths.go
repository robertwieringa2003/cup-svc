package cupsoup

const (
	PageDefault          = "Default.aspx"
	PageLoginWebForm     = "LogInWebForm.aspx"
	PagePrintableRooster = "PrintableRooster.aspx"
	PageRoosterForm      = "RoosterForm.aspx"

	ResCaptchaImage = "CaptchaImage.axd"
)
