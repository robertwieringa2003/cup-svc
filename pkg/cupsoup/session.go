package cupsoup

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var sessTokenRegxp = regexp.MustCompile(`^\(S\([a-z0-9]{24}\)\)$`)

type Session struct {
	ASPFields   ASPFields
	ExtraFields url.Values
	Token       string
	LastRequest time.Time
}

func (s Session) Valid() bool {
	return s.LastRequest.Add(3 * time.Hour).After(time.Now())
}

func (c *Client) Init(ctx context.Context) error {
	rsp, err := c.do(ctx, request{})
	if err != nil {
		return err
	}

	doc, err := goquery.NewDocumentFromReader(rsp.Body)
	if err != nil {
		return err
	}

	sessionToken, err := extractSessionToken(rsp.Request.URL.Path)
	if err != nil {
		return err
	}

	c.session = Session{
		Token:     sessionToken,
		ASPFields: ExtractASPFields(doc),
	}

	return nil
}

func (c *Client) Session() Session {
	return c.session
}

func extractSessionToken(cupURL string) (string, error) {
	parts := strings.Split(cupURL, "/")
	if len(parts) < 2 {
		return "", errors.New("expected a valid path after initial request")
	}

	tokenPart := parts[1]
	if !sessTokenRegxp.MatchString(tokenPart) {
		return "", fmt.Errorf("expected a session token, got %v", tokenPart)
	}
	return tokenPart, nil
}
