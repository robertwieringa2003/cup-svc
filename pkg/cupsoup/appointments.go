package cupsoup

import (
	"context"
	"net/http"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/parse"
)

const appointmentTableSelector = "#rooster > tbody > tr.cls0, #rooster > tbody > tr.cls1"

func (c *Client) PendingAppointments(ctx context.Context) ([]model.Appointment, error) {
	if !c.session.Valid() {
		return nil, ErrInvalidSession
	}

	doc, err := c.doDocumentRequest(ctx, request{
		method:           http.MethodGet,
		path:             PageRoosterForm,
		withoutASPFields: AllASPFields(),
	})
	if err != nil {
		return nil, err
	}

	tables := doc.Find(appointmentTableSelector)
	appointments := make([]model.Appointment, tables.Size())

	tables.EachWithBreak(func(i int, table *goquery.Selection) bool {
		var appointment model.Appointment

		// Don't shadow err so a check can be done after EachWithBreak has finished
		// whether an error occurred or not.
		appointment, err = parse.AppointmentTable(table)
		if err != nil {
			return false
		}

		appointments[i] = appointment

		return true
	})

	// An err might have been returned by parse.AppointmentTable, so we have to check.
	if err != nil {
		return nil, err
	}
	return appointments, nil
}
