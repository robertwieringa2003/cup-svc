package cupsoup

import (
	"fmt"
	"io"
	"net/http"
	"runtime"

	"github.com/rs/zerolog/log"
)

type roundTripper struct {
	callbacks    []func(r *http.Request)
	roundTripper http.RoundTripper
}

func (i roundTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	for _, cb := range i.callbacks {
		cb(r)
	}
	return i.roundTripper.RoundTrip(r)
}

func closeResponseBody(c io.Closer) {
	if err := c.Close(); err != nil {
		msg := "Could not close response body"

		pc, _, _, ok := runtime.Caller(1)
		details := runtime.FuncForPC(pc)
		if !ok && details != nil {
			msg += fmt.Sprintf(" (provided by %v)", details.Name())
		}

		log.Error().Err(err).Msg(msg)
	}
}
