package cupsoup

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/parse"
)

func (c *Client) Timetable(ctx context.Context) (model.Timetable, error) {
	if !c.session.Valid() {
		return nil, ErrInvalidSession
	}

	sel, err := c.getPrintableTimetableRange(ctx)
	if err != nil {
		return nil, err
	}

	doc, err := c.doDocumentRequest(ctx, request{
		method: http.MethodPost,
		path:   PagePrintableRooster,
		extraFields: url.Values{
			"dropDatumVan": {sel.min},
			"dropDatumTot": {sel.max},
		},
	})
	if err != nil {
		return nil, err
	}

	timetable, err := parse.PrintableTimetable(doc.Selection)
	if err != nil {
		return nil, fmt.Errorf("could not parse timetable: %w", err)
	}
	return timetable, nil
}

type selection struct {
	min string
	max string
}

func (c *Client) getPrintableTimetableRange(ctx context.Context) (selection, error) {
	// Make sure we are on the correct page to be able to 'click' the printable timetable button.
	rsp, err := c.do(ctx, request{
		method:           http.MethodGet,
		path:             PageRoosterForm,
		withoutASPFields: AllASPFields(),
	})
	if err != nil {
		return selection{}, err
	}
	defer closeResponseBody(rsp.Body)

	rspDoc, err := c.doDocumentRequest(ctx, request{
		method: http.MethodPost,
		path:   PageRoosterForm,
		extraFields: url.Values{
			"ToPrintableRooster": {"Printbaar Rooster"},
		},
	})
	if err != nil {
		return selection{}, err
	}

	min, exists := rspDoc.Find("#dropDatumVan > option").First().Attr("value")
	if !exists {
		return selection{}, errors.New("could not find minimum option")
	}

	max, exists := rspDoc.Find("#dropDatumTot > option").Last().Attr("value")
	if !exists {
		return selection{}, errors.New("could not find maximum option")
	}

	return selection{min, max}, nil
}
