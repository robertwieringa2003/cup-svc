package parse

import (
	"errors"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
)

func PreColumnImage(s *goquery.Selection) (model.PreColumnImageInfo, error) {
	srcAttr, srcAttrExists := s.Attr("src")
	omoAttr, omoAttrExists := s.Attr("onmouseover")

	if srcAttrExists {
		var msg string

		if omoAttrExists {
			var err error

			quoted := tillLast(fromFirst(omoAttr, "'"), "'")
			msg, err = unquote(quoted)
			if err != nil {
				return model.PreColumnImageInfo{}, err
			}
		}

		return model.PreColumnImageInfo{
			IsError: srcAttr == "images/infoError.png",
			Msg:     msg,
		}, nil
	}

	return model.PreColumnImageInfo{}, errors.New("given selection is no pre column image")
}
