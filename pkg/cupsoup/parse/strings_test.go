package parse

import (
	"errors"
	"testing"
)

func TestFromFirst(t *testing.T) {
	tests := map[string]struct {
		input  string
		prefix string
		want   string
	}{
		"empty_prefix":             {input: "blabla", prefix: "", want: "blabla"},
		"nonexistent_prefix":       {input: "blabla", prefix: "test", want: ""},
		"single_letter_prefix":     {input: "blabla", prefix: "a", want: "abla"},
		"multiple_char_prefix":     {input: "pumpkin pie", prefix: "in", want: "in pie"},
		"empty_input_and_prefix":   {input: "", prefix: "", want: ""},
		"prefix_at_start_of_input": {input: "test", prefix: "te", want: "test"},
	}

	for name := range tests {
		name := name
		tc := tests[name]

		t.Run(name, func(t *testing.T) {
			got := fromFirst(tc.input, tc.prefix)

			if tc.want != got {
				t.Fatalf("want != got (name: %q): want = %q, got = %q", name, tc.want, got)
			}
		})
	}
}

func TestTillLast(t *testing.T) {
	tests := map[string]struct {
		input  string
		suffix string
		want   string
	}{
		"empty_suffix":                        {input: "blabla", suffix: "", want: "blabla"},
		"multi_char_suffix":                   {input: "pumpkin pie", suffix: "in", want: "pumpkin"},
		"single_char_suffix":                  {input: "test", suffix: "s", want: "tes"},
		"single_letter_prefix":                {input: "blabla", suffix: "b", want: "blab"},
		"nonexistent_suffix":                  {input: "blabla", suffix: "test", want: "blabla"},
		"empty_input_and_suffix":              {input: "", suffix: "", want: ""},
		"multi_char_input_single_char_suffix": {input: "apple", suffix: "p", want: "app"},
	}

	for name := range tests {
		name := name
		tc := tests[name]

		t.Run(name, func(t *testing.T) {
			got := tillLast(tc.input, tc.suffix)

			if tc.want != got {
				t.Fatalf("want != got (name: %s): want = %q, got = %q", name, tc.want, got)
			}
		})
	}
}

func TestUnquote(t *testing.T) {
	tests := map[string]struct {
		input string
		want  string
		err   error
	}{
		"singly_quoted": {input: `'"test"'`, want: `"test"`},
		"doubly_quoted": {input: `"'test'"`, want: `'test'`},
	}

	for name := range tests {
		name := name
		tc := tests[name]

		t.Run(name, func(t *testing.T) {
			got, err := unquote(tc.input)
			if err != nil {
				if !errors.Is(err, tc.err) {
					t.Fatalf("err != tc.err; err=%v, tc.err=%v", err, tc.err)
				}
				return
			}

			if got != tc.want {
				t.Fatalf("got != want; got=%v, want=%v", got, tc.want)
			}
		})
	}
}
