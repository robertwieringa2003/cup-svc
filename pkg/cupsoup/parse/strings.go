package parse

import (
	"strconv"
	"strings"
)

func fromFirst(s, prefix string) string {
	if prefix == "" || s == "" {
		return s
	}

	var j int

	for i, c := range []byte(s) {
		if j == len(prefix) {
			if i < len(prefix) {
				return ""
			}
			return s[i-len(prefix):]
		}
		if c != prefix[j] {
			j = 0
		} else {
			j++
		}
	}

	return ""
}

func tillLast(s, suffix string) string {
	if suffix == "" || s == "" {
		return s
	}

	j := len(suffix) - 1

	for i := len(s) - 1; i >= 0; i-- {
		c := s[i]

		if c != suffix[j] {
			j = len(suffix) - 1
		} else {
			if j == 0 {
				return s[:i+len(suffix)]
			}
			j--
		}
	}

	return s
}

// unquote wraps the standard library's strings.unquote function,
// but replaces any single quote with a double quote if the string
// starts with a single quote, and also replaces existing double quotes
// to escaped double quotes, so the string is unquoted properly.
func unquote(s string) (string, error) {
	if strings.HasPrefix(s, "'") {
		s = strings.ReplaceAll(s, `"`, `\"`)
		s = strings.ReplaceAll(s, "'", `"`)
	}
	return strconv.Unquote(s)
}
