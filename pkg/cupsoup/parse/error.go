package parse

type parseError string

func (e parseError) Error() string {
	return string(e)
}

const (
	ErrNoData parseError = "no data"
)
