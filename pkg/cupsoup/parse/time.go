package parse

import "time"

const (
	dateLayout = "2-1-2006"
	timeLayout = "15.04"
)

func timeParse(layout, value string) (time.Time, error) {
	loc, err := time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		return time.Now(), err
	}

	return time.ParseInLocation(layout, value, loc)
}
