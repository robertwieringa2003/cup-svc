package cupsoup

import (
	"context"
	"fmt"
	"image"
	"image/jpeg"
	"io/ioutil"
	"net/http"
	"net/url"
)

func (c *Client) CaptchaImageRaw(ctx context.Context, guid string) ([]byte, error) {
	rsp, err := c.captchaImage(ctx, guid)
	if err != nil {
		return nil, fmt.Errorf("could not get captcha image: %w", err)
	}
	defer closeResponseBody(rsp.Body)

	bytes, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read captcha image response body: %w", err)
	}
	return bytes, nil
}

func (c *Client) CaptchaImage(ctx context.Context, guid string) (image.Image, error) {
	rsp, err := c.captchaImage(ctx, guid)
	if err != nil {
		return nil, fmt.Errorf("could not get captcha image: %w", err)
	}
	defer closeResponseBody(rsp.Body)

	img, err := jpeg.Decode(rsp.Body)
	if err != nil {
		return nil, fmt.Errorf("could not decode captcha image: %w", err)
	}
	return img, nil
}

func (c *Client) captchaImage(ctx context.Context, guid string) (*http.Response, error) {
	rsp, err := c.do(ctx, request{
		method: http.MethodGet,
		path:   ResCaptchaImage,
		extraFields: url.Values{
			"guid": {guid},
		},
		withoutASPFields: AllASPFields(),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
