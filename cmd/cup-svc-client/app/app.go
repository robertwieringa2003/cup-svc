//nolint
package app

import (
	"bufio"
	"context"
	"encoding/json"
	"log"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc"

	pb "gitlab.com/timeterm/proto/go/cup"
)

// Please ignore the mess as much as possible.
func Execute() {
	start := time.Now()

	conn, err := grpc.Dial("localhost:12345", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not dial server: %v", err)
	}
	defer func() {
		err = conn.Close()
		if err != nil {
			log.Fatalf("Could not close connection: %v", err)
		}
	}()

	client := pb.NewCUPClient(conn)

	_, err = client.SetUser(context.Background(), &pb.SetUserRequest{
		TtSessionId: "16204",
		CupName:     "Broekhoff  Rutger (gv5c)",
	})
	if err != nil {
		log.Printf("Error creating user: %v", err)
		return
	}

	ustart := time.Now()
	u, err := client.GetUser(context.Background(), &pb.GetUserRequest{
		TtSessionId: "16204",
	})
	if err != nil {
		log.Printf("Error getting user: %v", err)
		return
	}
	log.Printf("CupName: %#v", u.GetCupName())
	log.Printf("Took: %v (total %v)", time.Since(ustart), time.Since(start))

	cl, err := client.SignIn(context.Background(), &pb.SignInRequest{
		TtSessionId: "16204",
		Pin:         "1234",
	})
	if err != nil {
		log.Printf("Error signing in: %v", err)
		return
	}

	var sess *pb.Session
	var candidate string
	var e bool
	for {
		var ev *pb.SignInEvent
		ev, err = cl.Recv()
		if err != nil {
			log.Printf("Error signing in: %v", err)

			trailer := cl.Trailer()
			if len(trailer["cup-captcha-url"]) > 0 {
				log.Printf("CUP captcha URL: %v", trailer["cup-captcha-url"][0])
				e = true //nolint:staticcheck,ineffassign
				break
			}

			return
		}

		log.Printf("Sign in event: %v", ev)

		switch ev.GetType() {
		case pb.SE_SIGNING_IN:
			candidate = ev.GetSelectedOpt() //nolint:staticcheck,ineffassign
		case pb.SE_FAILED:
			sess = ev.GetSession()
		case pb.SE_SIGNED_IN:
			log.Printf("Signed in successfully: URL: https://ccgobb.cupweb6.nl/%v/RoosterForm.aspx",
				ev.GetSession().GetToken(),
			)
			sess = ev.GetSession()
		}
	}

	if e {
		s := bufio.NewScanner(os.Stdin)
		s.Scan()
		if err = s.Err(); err != nil {
			log.Printf("Error getting Captcha text: %v", err)
			return
		}

		cl, err = client.SignInRetry(context.Background(), &pb.SignInRetryRequest{
			Pin:         "1234",
			CaptchaText: strings.TrimSpace(s.Text()),
			SelectedOpt: candidate,
			Session:     sess,
		})
		if err != nil {
			log.Printf("Error signing in (retry): %v", err)
			return
		}

		for {
			var ev *pb.SignInEvent
			ev, err = cl.Recv()
			if err != nil {
				log.Printf("Error signing in (retry): %v", err)

				trailer := cl.Trailer()
				if len(trailer["cup-captcha-url"]) > 0 {
					log.Printf("CUP captcha URL: %v", trailer["cup-captcha-url"][0])
					e = true //nolint:staticcheck,ineffassign
					break
				}

				return
			}

			log.Printf("Sign in event (retry): %v", ev)

			switch ev.GetType() {
			case pb.SE_SIGNING_IN:
				candidate = ev.GetSelectedOpt() //nolint:staticcheck,ineffassign
			case pb.SE_FAILED:
				sess = ev.GetSession()
			case pb.SE_SIGNED_IN:
				log.Printf("Signed in successfully (retry): URL: https://ccgobb.cupweb6.nl/%v/RoosterForm.aspx",
					ev.GetSession().GetToken(),
				)
				sess = ev.GetSession()
			}
		}
	}

	rsp, err := client.PendingAppointments(context.Background(), &pb.PendingAppointmentsRequest{Session: sess})
	if err != nil {
		log.Printf("Error getting pending appointments: %v", err)
		return
	}

	sess = rsp.Session

	err = json.NewEncoder(os.Stdout).Encode(rsp.Appointments)
	if err != nil {
		log.Printf("An error occurred dumping appointments %v", err)
		return
	}

	_, err = client.Choose(context.Background(), &pb.ChooseRequest{
		Session:    sess,
		InternalId: 53,
	})
	if err != nil {
		log.Printf("Error choosing: %v", err)
		return
	}

	log.Printf("Entire procedure took %v", time.Since(start))
}
