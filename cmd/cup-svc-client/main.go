package main

import (
	_ "github.com/joho/godotenv/autoload"

	"gitlab.com/timeterm/cup-svc/cmd/cup-svc-client/app"
)

func main() {
	app.Execute()
}
